const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator');
const functions = require('firebase-functions');

var myApp = express();
myApp.use(bodyParser.urlencoded({extended:false}));
myApp.use(bodyParser.json());
myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');

exports.myApp = functions.https.onRequest(myApp);

myApp.get('/',function(req, res){
    res.render('main');
});
myApp.get('/AngelsTeaInvoice',function(req, res){
    res.render('main');
});
myApp.post('/AngelsTeaInvoice',[
    //Validate user information inputs 
    check('greenteaQuantity').custom(value =>{
        intvalue = parseInt(value)
        if(isNaN(intvalue) && value != ""){
            throw new Error('Green Tea: Only Number please');
        }
        return true;
    }), 
    check('orangepekoeQuantity').custom(value =>{
        intvalue = parseInt(value)
        if(isNaN(intvalue) && value != ""){
            throw new Error('Orange Pekoe: Only Number please');
        }
        return true;
    }), 
    check('rooibosQuantity').custom(value =>{
        intvalue = parseInt(value)
        if(isNaN(intvalue) && value != ""){
            throw new Error('Rooibos: Only Number please');
        }
        return true;
    }), 
    //error message on if the user input is negative number
    check('greenteaQuantity').custom(value =>{
        value = parseInt(value)
        if(value < 0){
            throw new Error('Green Tea: Only Positive Number please');
        }
        return true;
    }),
    check('orangepekoeQuantity').custom(value =>{
        value = parseInt(value)
        if(value < 0){
            throw new Error('Orange Pekoe: Only Positive Number please');
        }
        return true;
    }),
    check('rooibosQuantity').custom(value =>{
        value = parseInt(value)
        if(value < 0){
            throw new Error('Rooibos: Only Positive Number please');
        }
        return true;
    }),
    check('greenteaQuantity', 'orangepekoeQuantity', 'rooibosQuantity').custom(value =>{
        value = parseInt(value)
        if(isNaN(value) || value == 0){
            throw new Error('Please choose at least one product');
        }
        return true;
    }),
    check('name', 'Please enter Name').not().isEmpty(), //error message on when it IS empty
    check('name').custom(value =>{
        var nameRegex = /^[A-Za-z\-\s\']+$/;
        if(!nameRegex.test(value)){
            throw new Error('Name is not in the correct format');
        }
        return true;
    }),
    check('email', 'Please enter Email address').not().isEmpty(), 
    check('email').custom(value =>{
        var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(!emailRegex.test(value)){
            throw new Error('Email is not in the correct format');
        }
        return true;
    }),
    check('phone', 'Please enter Phone number').not().isEmpty(),
    check('phone').custom(value =>{
        var phoneRegex = /^[0-9]{3}\-?[0-9]{3}\-?[0-9]{4}$/;
        if(!phoneRegex.test(value)){
            throw new Error('Phone number is not in the correct format');
        }
        return true;
    }),
    check('address', 'Please enter Address').not().isEmpty(),
    check('city', 'Please enter City').not().isEmpty(),
    check('province', 'Please select Province').not().isEmpty(),
    check('postcode', 'Please enter Postal code').not().isEmpty(),
    check('postcode').custom(value =>{
        var postcodeRegex = /^[A-Za-z][0-9][A-Za-z]\s?[0-9][A-Za-z][0-9]$/;
        if(!postcodeRegex.test(value)){
            throw new Error('Postal code is not in the correct format');
        }
        return true;
    }),  
    check('deliverytime', 'Please select Delivery time').not().isEmpty()

],function(req, res){
    const errors = validationResult(req);
    
    var greenteaQuantity = parseInt(req.body.greenteaQuantity) || 0;
    var orangepekoeQuantity = parseInt(req.body.orangepekoeQuantity) || 0;
    var rooibosQuantity = parseInt(req.body.rooibosQuantity) || 0;

    if(!errors.isEmpty() || (greenteaQuantity + orangepekoeQuantity + rooibosQuantity) == 0 ){
        var errorsData = {
            errors: errors.array(),
        }
        res.render('main', errorsData);
    }
    else{
        //Calculate Subtotal for each product 
        if(greenteaQuantity >= 0){
            greenteaSubtotal = greenteaQuantity * 6.50;
        }
        if(orangepekoeQuantity >= 0){
            orangepekoeSubtotal = orangepekoeQuantity * 5.50;
        }
        if(rooibosQuantity >= 0){
            rooibosSubtotal = rooibosQuantity * 7.50;   
        }

        var subtotal = greenteaSubtotal + orangepekoeSubtotal + rooibosSubtotal;

        //User inputs for personal information
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var address = req.body.address;
        var city = req.body.city;
        var province = req.body.province;
        var postcode = req.body.postcode;
        var deliverytime = req.body.deliverytime;

        //Shipping costs for different delivery time
        var oneDayShipping = 30.00;
        var twoDayShipping = 25.00;
        var threeDayShipping = 20.00;
        var fourDayShipping = 15.00;
        var shippingCost = 0.00;
        var tax = 0.00;

        // Delivery(Shipping) Cost
        switch(deliverytime){
            case "1":
                shippingCost = oneDayShipping;
                break;
            case "2":
                shippingCost = twoDayShipping;
                break;
            case "3":
                shippingCost = threeDayShipping;
                break;
            case "4":
                shippingCost = fourDayShipping;
                break;
        }        

        //Tax rates for each province
        var taxAlberta = 0.05;
        var taxBritishColumbia = 0.12;
        var taxManitoba = 0.12;
        var taxNewBrunswick = 0.15;
        var taxNewfoundland = 0.15;
        var taxNovaScotia = 0.15;
        var taxOntario = 0.13;
        var taxPEI = 0.15;
        var taxQuebec = 0.14975;
        var taxSaskatchewan = 0.11;
        var taxNunavut = 0.05;
        var taxNorthwest = 0.05;
        var taxYukon = 0.05;

        //Tax calculation 
        switch(province){
            case 'Alberta':
                tax = (subtotal + shippingCost) * taxAlberta;
                break;
            case 'British Columbia' :
                tax = (subtotal + shippingCost) * taxBritishColumbia;
                break;
            case 'Manitoba' :
                tax = (subtotal + shippingCost) * taxManitoba;
                break;
            case 'New Brunswick' :
                tax = (subtotal + shippingCost) * taxNewBrunswick;
                break;
            case 'NewFoundland' : 
                tax = (subtotal + shippingCost) * taxNewfoundland;
                break;
            case 'Nova Scotia' :
                tax = (subtotal + shippingCost) * taxNovaScotia;
                break;
            case 'Ontario':
                tax = (subtotal + shippingCost) * taxOntario;
                break;
            case 'Prince Edward Island' :
                tax = (subtotal + shippingCost) * taxPEI;
                break;
            case 'Quebec' :
                tax = (subtotal + shippingCost) * taxQuebec;
                break;
            case 'Saskathewan' :
                tax = (subtotal + shippingCost) * taxSaskatchewan;
                break;
            case 'Nunavut' :
                tax = (subtotal + shippingCost) * taxNunavut;
                break;
            case 'Northwest' :
                tax = (subtotal + shippingCost) * taxNorthwest;
                break;
            case 'Yukon' :
                tax = (subtotal + shippingCost) * taxYukon;
                break; 
        }
        
        //Total calculation         
        var total = subtotal + shippingCost + tax;   
        
        //Outputs
        var pageData = {
           
            greenteaQuantity: greenteaQuantity,
            orangepekoeQuantity: orangepekoeQuantity,
            rooibosQuantity: rooibosQuantity,
            greenteaSubtotal: greenteaSubtotal,
            orangepekoeSubtotal: orangepekoeSubtotal,
            rooibosSubtotal: rooibosSubtotal, 

            name: name, 
            email: email,
            phone: phone, 
            address: address, 
            city: city,
            province: province,
            postcode: postcode, 
            deliverytime: deliverytime, 
            subtotal: subtotal,
            shippingCost: shippingCost,
            tax: tax,
            total: total

        };
        res.render('invoice', pageData);
    }
});

myApp.listen(8080);
console.log('Server started at 8080 for mywebsite...');
